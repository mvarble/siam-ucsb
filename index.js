/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/server/app.js":
/*!***************************!*\
  !*** ./src/server/app.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! body-parser */ \"body-parser\");\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var morgan__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! morgan */ \"morgan\");\n/* harmony import */ var morgan__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(morgan__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var passport__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! passport */ \"passport\");\n/* harmony import */ var passport__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(passport__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _sessions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sessions */ \"./src/server/sessions.js\");\n/* harmony import */ var _auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth */ \"./src/server/auth.js\");\n/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./routes */ \"./src/server/routes.js\");\n/* harmony import */ var _filestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./filestore */ \"./src/server/filestore.js\");\n/* harmony import */ var _db__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./db */ \"./src/server/db.js\");\n/**\n * server/app.js\n *\n * This module exports the Express webapp\n */\n// module dependencies: npm packages\n\n\n\n\n // module dependencies: project modules\n\n\n\n\n\n // create the Express app\n\nvar app = express__WEBPACK_IMPORTED_MODULE_0___default()(); // body-parser\n\napp.use(body_parser__WEBPACK_IMPORTED_MODULE_1___default.a.json());\napp.use(body_parser__WEBPACK_IMPORTED_MODULE_1___default.a.urlencoded({\n  extended: false\n})); // logger middleware\n\napp.use(morgan__WEBPACK_IMPORTED_MODULE_2___default()('tiny')); // session middleware\n\napp.use(Object(_sessions__WEBPACK_IMPORTED_MODULE_5__[\"default\"])()); // authentication middleware\n\napp.use(passport__WEBPACK_IMPORTED_MODULE_3___default.a.initialize());\napp.use(passport__WEBPACK_IMPORTED_MODULE_3___default.a.session());\napp.use('/auth', Object(_auth__WEBPACK_IMPORTED_MODULE_6__[\"authRouter\"])()); // static middleware\n\napp.use('/static/uploads', Object(_filestore__WEBPACK_IMPORTED_MODULE_8__[\"default\"])());\napp.use('/static', express__WEBPACK_IMPORTED_MODULE_0___default.a[\"static\"](path__WEBPACK_IMPORTED_MODULE_4___default.a.join(process.cwd(), 'dist')));\napp.use('/static/*', function (req, res) {\n  return res.status(404).send('404');\n}); // database API middleware\n\napp.use('/db', Object(_db__WEBPACK_IMPORTED_MODULE_9__[\"default\"])()); // routes\n\napp.use('/', Object(_routes__WEBPACK_IMPORTED_MODULE_7__[\"default\"])());\n/* harmony default export */ __webpack_exports__[\"default\"] = (app);\n\n//# sourceURL=webpack:///./src/server/app.js?");

/***/ }),

/***/ "./src/server/auth.js":
/*!****************************!*\
  !*** ./src/server/auth.js ***!
  \****************************/
/*! exports provided: loggedIn, authorized, authRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"loggedIn\", function() { return loggedIn; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"authorized\", function() { return authorized; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"authRouter\", function() { return authRouter; });\n/* harmony import */ var sqlite3__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sqlite3 */ \"sqlite3\");\n/* harmony import */ var sqlite3__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sqlite3__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var passport__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! passport */ \"passport\");\n/* harmony import */ var passport__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(passport__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var passport_local__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! passport-local */ \"passport-local\");\n/* harmony import */ var passport_local__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(passport_local__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! crypto */ \"crypto\");\n/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(crypto__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var randomstring__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! randomstring */ \"randomstring\");\n/* harmony import */ var randomstring__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(randomstring__WEBPACK_IMPORTED_MODULE_6__);\n/**\n * server/auth.js\n *\n * This module exports middleware responsible for authenticating users.\n * It uses a local passport strategy and stores the user data into the sqlite3\n * database located at ../../database.db\n */\n// module dependencies: npm packages\n\n\n\n\n\n\n // connect to the sqlite3 database\n\nvar db = new sqlite3__WEBPACK_IMPORTED_MODULE_0__[\"Database\"](path__WEBPACK_IMPORTED_MODULE_4___default.a.join(process.cwd(), 'database.db')); // this function uses crypto to hash passwords\n\nvar hashPassword = function hashPassword(password, salt) {\n  var hash = crypto__WEBPACK_IMPORTED_MODULE_5___default.a.createHash('sha256');\n  hash.update(password);\n  hash.update(salt);\n  return hash.digest('hex');\n}; // configure passport\n\n\npassport__WEBPACK_IMPORTED_MODULE_1___default.a.use(new passport_local__WEBPACK_IMPORTED_MODULE_2___default.a(function (username, password, done) {\n  // get the salts of the users with matching username\n  db.get('SELECT salt FROM users WHERE username = ?', username, function (err, user) {\n    // if we don't have any matches, call it a day\n    if (err || !user) {\n      return done(null, false);\n    } // otherwise, get the password hash and see if there is a corresponding \n    // user\n\n\n    db.get('SELECT username, id, auth FROM users WHERE username = ? AND password = ?', username, hashPassword(password, user.salt), function (err, user) {\n      // if there is a user, return\n      if (err || !user) {\n        return done(null, false);\n      }\n\n      return done(null, user);\n    });\n  });\n}));\npassport__WEBPACK_IMPORTED_MODULE_1___default.a.serializeUser(function (user, done) {\n  return done(null, user);\n});\npassport__WEBPACK_IMPORTED_MODULE_1___default.a.deserializeUser(function (user, done) {\n  return done(null, user);\n}); // a session checker\n\nvar loggedIn = function loggedIn(req) {\n  return req.session && req.session.passport && req.session.passport.user ? true : false;\n};\n\n // an authorization checker\n\nvar authorized = function authorized(req) {\n  return loggedIn(req) && req.session.passport.user.auth ? true : false;\n};\n\n // create router middleware\n\nvar authRouter = function authRouter() {\n  // the router we will eventually return\n  var router = Object(express__WEBPACK_IMPORTED_MODULE_3__[\"Router\"])(); // We use an almost identical form for the login and create pages\n\n  var form = function form(buttonText, onsubmit) {\n    return \"\\n<!DOCTYPE html>\\n<html>\\n<head>\\n  <meta charset=\\\"utf-8\\\">\\n  <title>User \".concat(buttonText, \"</title>\\n  <meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1.0\\\">\\n  <link href=\\\"https://webfonts.brand.ucsb.edu/webfont.min.css\\\" rel=\\\"stylesheet\\\">\\n</head>\\n<body>\\n  <form method=\\\"post\\\" onsubmit=\\\"\").concat(onsubmit, \"\\\">\\n    <input type=\\\"text\\\" name=\\\"username\\\" required>\\n    <input type=\\\"password\\\" name=\\\"password\\\" required>\\n    <button type=\\\"submit\\\">\").concat(buttonText, \"</button>\\n  </form>\\n</body>\\n</html>\\n\");\n  };\n\n  var authorizationMessage = \"It appears this user is not authorized. \\n    Send an email to Matthew Varble at \\n    <a href=\\\"mailto:mvarble@math.ucsb.edu\\\">mvarble@math.ucsb.edu</a> \\n    to request elevated priviledges with this account.\\n  \"; // home page for authorized users\n\n  router.get('/', function (req, res) {\n    if (!loggedIn(req)) return res.redirect('/auth/login');\n    if (!authorized(req)) return res.send(authorizationMessage);\n    return res.send('<a href=\"/auth/users\">Users</a>');\n  }); // the page listing all users\n\n  router.get('/users', function (req, res, next) {\n    if (!loggedIn(req)) return res.redirect('/auth/login');\n    if (!authorized(req)) return res.send(authorizationMessage);\n    db.all('SELECT id, username, auth FROM users', function (err, users) {\n      if (err) return next(err);\n      var usersString = users.map(function (_ref) {\n        var id = _ref.id,\n            username = _ref.username,\n            auth = _ref.auth;\n        return \"\".concat(id, \": \").concat(username, \"; \").concat(auth);\n      }).join('</li><li>');\n      res.send(\"<ul><li>\".concat(usersString, \"</li></ul>\"));\n    });\n  }); // configure the create user route\n\n  router.get('/create', function (req, res) {\n    if (loggedIn(req)) return res.redirect('/');\n    return res.send(form('Create', 'createUser(event)'));\n  });\n  router.post('/create', function (req, res) {\n    var _req$body = req.body,\n        username = _req$body.username,\n        password = _req$body.password;\n    var salt = randomstring__WEBPACK_IMPORTED_MODULE_6___default.a.generate();\n    db.run('INSERT INTO users(username, password, salt, auth) VALUES(?, ?, ?, 0)', [username, hashPassword(password, salt), salt], function (err) {\n      if (err) {\n        return res.status(422).send(err.message);\n      }\n\n      return res.redirect('/');\n    });\n  }); // configure the login\n\n  router.get('/login', function (req, res) {\n    if (loggedIn(req)) {\n      return res.redirect('/');\n    }\n\n    return res.send(form('Login', 'loginUser(event)'));\n  });\n  router.post('/login', passport__WEBPACK_IMPORTED_MODULE_1___default.a.authenticate('local'), function (req, res) {\n    return res.redirect('/');\n  }); // configure the logout\n\n  router.get('/logout', function (req, res) {\n    req.session.destroy(function (err) {\n      if (err) return next(err);\n      return res.redirect('/');\n    });\n  }); // return the router\n\n  return router;\n};\n\n\n\n//# sourceURL=webpack:///./src/server/auth.js?");

/***/ }),

/***/ "./src/server/db.js":
/*!**************************!*\
  !*** ./src/server/db.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var sqlite3__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sqlite3 */ \"sqlite3\");\n/* harmony import */ var sqlite3__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sqlite3__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var url__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! url */ \"url\");\n/* harmony import */ var url__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(url__WEBPACK_IMPORTED_MODULE_3__);\nfunction _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }\n\nfunction _nonIterableSpread() { throw new TypeError(\"Invalid attempt to spread non-iterable instance\"); }\n\nfunction _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === \"[object Arguments]\") return Array.from(iter); }\n\nfunction _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }\n\n/**\n * server/db.js\n *\n * This module is responsible for creating the SQLITE database API\n */\n// module dependencies: npm packages\n\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  // router we eventually export\n  var router = Object(express__WEBPACK_IMPORTED_MODULE_0__[\"Router\"])(); // connect to the sqlite3 database\n\n  var db = new sqlite3__WEBPACK_IMPORTED_MODULE_1__[\"Database\"](path__WEBPACK_IMPORTED_MODULE_2___default.a.join(process.cwd(), 'database.db')); // a simple function we repeatedly use\n\n  var cb = function cb(res, next, err) {\n    if (err) return next(err);\n    res.send();\n  }; // posting pages\n\n\n  router.post('/pages', function (req, res, next) {\n    // extract the body if it is sufficient\n    if (!req.body) return next(Error('The post request to /pages has no body'));\n    var _req$body = req.body,\n        title = _req$body.title,\n        path = _req$body.path,\n        body = _req$body.body,\n        position = _req$body.position;\n\n    if (!title || !path || !body || !position) {\n      return next(Error('The post request to /pages has an insufficient body'));\n    } // post it to the database\n\n\n    db.run('INSERT INTO pages (title, path, body, position) VALUES (?, ?, ?, ?)', [title, path, body, position], function (err) {\n      return cb(res, next, err);\n    });\n  }); // updating pages\n\n  router.put('/pages', function (req, res, next) {\n    // check if there is a body\n    if (!req.body) return next(Error('The put request to /pages has no body'));\n    if (!req.body.id) return next(Error('The put request to /pages has no id')); // get the fields that are requesting to be changed from the request\n\n    var _req$body2 = req.body,\n        id = _req$body2.id,\n        title = _req$body2.title,\n        path = _req$body2.path,\n        body = _req$body2.body,\n        position = _req$body2.position;\n    var bodyObj = {\n      title: title,\n      path: path,\n      body: body,\n      position: position\n    };\n    var keys = Object.keys(bodyObj).filter(function (k) {\n      return bodyObj[k];\n    }); // if there are no keys, it was a blank request\n\n    if (!keys.length) return res.send(); // update any non-blank request\n\n    var setString = keys.map(function (k) {\n      return \"\".concat(k, \" = (?)\");\n    }).join(', ');\n    db.run(\"UPDATE pages SET \".concat(setString, \" WHERE id = (?)\"), [].concat(_toConsumableArray(keys.map(function (k) {\n      return bodyObj[k];\n    })), [id]), function (err) {\n      return cb(res, next, err);\n    });\n  }); // deleting pages\n\n  router[\"delete\"]('/pages', function (req, res, next) {\n    // check if there is a body\n    var noId = 'The delete request to /pages has no id';\n    if (!req.body || !req.body.id) return next(Error(noId)); // if so, try to delete the field\n\n    var id = req.body.id;\n    db.run(\"DELETE FROM pages WHERE id = (?)\", id, function (err) {\n      return cb(res, next, err);\n    });\n  }); // getting talks\n\n  router.get('/talks', function (req, res, next) {\n    // get the query\n    var query = Object(url__WEBPACK_IMPORTED_MODULE_3__[\"parse\"])(req.url, true).query; // our callback\n\n    var callback = function callback(err, talks) {\n      if (err) return next(err);\n      res.set('Content-Type', 'application/json');\n      res.send(JSON.stringify(talks));\n    }; // if there is no begin/end, grab all of them\n\n\n    if (!query.begin || !query.end) {\n      db.all('SELECT * FROM talks', callback);\n    } else {\n      db.all('SELECT * FROM talks WHERE date >= ? AND date <= ? ORDER BY date', [query.begin, query.end], callback);\n    }\n  }); // posting talks\n\n  router.post('/talks', function (req, res, next) {\n    // check if there is a body\n    if (!req.body) return next(Error('The post request to /talks has no body')); // parse the body\n\n    var _req$body3 = req.body,\n        title = _req$body3.title,\n        speaker = _req$body3.speaker,\n        _abstract = _req$body3[\"abstract\"],\n        date = _req$body3.date;\n    var bodyObj = {\n      speaker: speaker,\n      title: title,\n      \"abstract\": _abstract,\n      date: date\n    };\n    var keys = Object.keys(bodyObj).filter(function (k) {\n      return bodyObj[k];\n    }); // if no keys, insert default\n\n    if (!keys.length) {\n      db.run('INSERT INTO talks DEFAULT VALUES', [], function (err) {\n        return cb(res, next, err);\n      });\n    } else {\n      // create the row\n      var setString = keys.join(', ');\n      db.run(\"INSERT INTO talks (\".concat(keys.join(', '), \") VALUES \\n        (\").concat(keys.map(function (k) {\n        return '?';\n      }).join(', '), \")\"), keys.map(function (k) {\n        return bodyObj[k];\n      }), function (err) {\n        return cb(res, next, err);\n      });\n    }\n  }); // updating talks\n\n  router.put('/talks', function (req, res, next) {\n    // check if there is a body\n    if (!req.body) return next(Error('The put request to /talks has no body'));\n    if (!req.body.id) return next(Error('The put request to /talks has no id')); // get the fields that are requesting to be changed from the request\n\n    var _req$body4 = req.body,\n        id = _req$body4.id,\n        title = _req$body4.title,\n        speaker = _req$body4.speaker,\n        _abstract2 = _req$body4[\"abstract\"],\n        date = _req$body4.date;\n    var bodyObj = {\n      title: title,\n      speaker: speaker,\n      \"abstract\": _abstract2,\n      date: date\n    };\n    var keys = Object.keys(bodyObj).filter(function (k) {\n      return bodyObj[k];\n    }); // if there are no keys, it was a blank request\n\n    if (!keys.length) return res.send(); // update any non-blank request\n\n    var setString = keys.map(function (k) {\n      return \"\".concat(k, \" = (?)\");\n    }).join(', ');\n    db.run(\"UPDATE talks SET \".concat(setString, \" WHERE id = (?)\"), [].concat(_toConsumableArray(keys.map(function (k) {\n      return bodyObj[k];\n    })), [id]), function (err) {\n      return cb(res, next, err);\n    });\n  }); // deleting talks\n\n  router[\"delete\"]('/talks', function (req, res, next) {\n    // check if there is a body\n    var noId = 'The delete request to /talks has no id';\n    if (!req.body || !req.body.id) return next(Error(noId)); // if so, try to delete the field\n\n    var id = req.body.id;\n    db.run(\"DELETE FROM talks WHERE id = (?)\", id, function (err) {\n      return cb(res, next, err);\n    });\n  });\n  return router;\n});\n\n//# sourceURL=webpack:///./src/server/db.js?");

/***/ }),

/***/ "./src/server/filestore.js":
/*!*********************************!*\
  !*** ./src/server/filestore.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var express_file_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express-file-store */ \"express-file-store\");\n/* harmony import */ var express_file_store__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express_file_store__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_2__);\n/**\n * server/filestore.js\n *\n * This exports middleware that will allow for upload/download of files\n */\n// module dependencies: npm packages\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  // our router\n  var router = Object(express__WEBPACK_IMPORTED_MODULE_0__[\"Router\"])(); // the filesystem path for storing the files\n\n  var fileDir = path__WEBPACK_IMPORTED_MODULE_2___default.a.join(process.cwd(), 'dist', 'uploads'); // the routes are handld by express-file-store\n\n  router.use(express_file_store__WEBPACK_IMPORTED_MODULE_1___default()('fs', {\n    path: fileDir\n  }).routes); // add an 404 handler\n\n  router.use(function (err, req, res, next) {\n    if (err && err.message === 'File not found') {\n      return res.status(404).send('FILE NOT FOUND');\n    }\n\n    return next(err);\n  });\n  return router;\n});\n\n//# sourceURL=webpack:///./src/server/filestore.js?");

/***/ }),

/***/ "./src/server/http.js":
/*!****************************!*\
  !*** ./src/server/http.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! http */ \"http\");\n/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_0__);\n/**\n * server/http.js\n *\n * This module exports the function which deploys an http server with an\n * Express webapp bound.\n */\n// module dependencies: npm packages\n // the function which deploys the server\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (app) {\n  // set the port of the webapp\n  app.set('port', 8666); // create the http server\n\n  var server = http__WEBPACK_IMPORTED_MODULE_0___default.a.createServer(app); // our logger\n\n  var log = function log(s) {\n    return process.stdout.write(\"\".concat(s, \"\\n\"));\n  }; // handle port errors by scooting up a port\n\n\n  server.on('error', function (err) {\n    if (err.code === 'EADDRINUSE') {\n      log(\"Port unavailable. Are you sure this service isn't already running?\");\n    }\n  }); // log server details when all works\n\n  server.on('listening', function () {\n    log(\"\".concat('='.repeat(80)));\n    log('SIAM Server');\n    log(\"Server is listening on port \".concat(server.address().port));\n    log(\"\".concat('='.repeat(80)));\n  }); // start the server\n\n  server.listen(8666);\n});\n\n//# sourceURL=webpack:///./src/server/http.js?");

/***/ }),

/***/ "./src/server/index.js":
/*!*****************************!*\
  !*** ./src/server/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http */ \"./src/server/http.js\");\n/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app */ \"./src/server/app.js\");\n/**\n * server/index.js\n *\n * This is the entry point for deploying the http server which serves our \n * webapp.\n */\n// module dependencies: project modules\n\n // deploy the server\n\nObject(_http__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(_app__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n\n//# sourceURL=webpack:///./src/server/index.js?");

/***/ }),

/***/ "./src/server/routes.js":
/*!******************************!*\
  !*** ./src/server/routes.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var sqlite3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sqlite3 */ \"sqlite3\");\n/* harmony import */ var sqlite3__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sqlite3__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-dom/server */ \"react-dom/server\");\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var url__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! url */ \"url\");\n/* harmony import */ var url__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(url__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth */ \"./src/server/auth.js\");\n/* harmony import */ var _util_markdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../util/markdown */ \"./src/util/markdown.js\");\n/* harmony import */ var _util_react_components__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../util/react-components */ \"./src/util/react-components.jsx\");\nfunction _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }\n\nfunction ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nfunction _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }\n\nfunction _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }\n\n/**\n * server/routes.js\n *\n * This module creates the router middleware for pages the user will visit.\n */\n// module dependencies: npm packages\n\n\n\n\n\n // module dependencies: project\n\n\n\n // this generates raw html, provided page data\n\nvar generateHtml = function generateHtml(page, pages, authState) {\n  var body = page.body,\n      title = page.title,\n      otherProps = _objectWithoutProperties(page, [\"body\", \"title\"]);\n\n  var props = _objectSpread({}, otherProps, {}, authState, {\n    pages: pages\n  });\n\n  var markup = _util_markdown__WEBPACK_IMPORTED_MODULE_7__[\"default\"].render(body);\n  return \"<!DOCTYPE html>\\n<html>\\n<head>\\n  <meta charset=\\\"utf-8\\\">\\n  <title>\".concat(title, \" | SIAM UCSB</title>\\n  <meta name=\\\"viewport\\\" content=\\\"width=device-width, initial-scale=1.0\\\">\\n  <link href=\\\"https://webfonts.brand.ucsb.edu/webfont.min.css\\\" rel=\\\"stylesheet\\\">\\n  <link href=\\\"/static/css/bundle.css\\\" rel=\\\"stylesheet\\\">\\n</head>\\n<body>\\n  <div id=\\\"root\\\">\").concat(Object(react_dom_server__WEBPACK_IMPORTED_MODULE_4__[\"renderToString\"])(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_util_react_components__WEBPACK_IMPORTED_MODULE_8__[\"default\"], _extends({\n    body: markup\n  }, props))), \"</div>\\n  <script src=\\\"/static/js/bundle.js\\\"></script>\\n</body>\\n</html>\\n\");\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  // the router we eventually return\n  var router = Object(express__WEBPACK_IMPORTED_MODULE_1__[\"Router\"])(); // every route at this point will be dealt with in our SQLITE database.\n\n  var db = new sqlite3__WEBPACK_IMPORTED_MODULE_2__[\"Database\"](path__WEBPACK_IMPORTED_MODULE_3___default.a.join(process.cwd(), 'database.db')); // talks model view\n\n  router.get('/talks', function (req, res, next) {\n    // create the authorization state\n    var state = {\n      loggedIn: Object(_auth__WEBPACK_IMPORTED_MODULE_6__[\"loggedIn\"])(req),\n      authorized: Object(_auth__WEBPACK_IMPORTED_MODULE_6__[\"authorized\"])(req)\n    }; // if there is no id query, we will assume that this may be handled by \n    // user-generated route\n\n    var id = Object(url__WEBPACK_IMPORTED_MODULE_5__[\"parse\"])(req.url, true).query.id;\n    if (!id) return next(); // perform a search for the talk on the database\n\n    db.get('SELECT date, speaker, title, abstract FROM talks WHERE id = ?', id, function (err, talk) {\n      // deal with errors / 404\n      if (err) return next(err);\n      if (!talk) return res.redirect('/404'); // otherwise, build the page\n\n      db.all('SELECT id, title, path FROM pages WHERE position >= 0 ORDER BY position;', function (err, pages) {\n        // errors go to error parser\n        if (err) return next(err); // make the page\n\n        res.send(generateHtml({\n          id: -1,\n          title: talk.title,\n          position: -1,\n          body: _util_markdown__WEBPACK_IMPORTED_MODULE_7__[\"default\"].render(\"# SIAM Seminar: \".concat(talk.date || '')) + _util_markdown__WEBPACK_IMPORTED_MODULE_7__[\"default\"].render(\"**Speaker.** \".concat(talk.speaker || '')) + _util_markdown__WEBPACK_IMPORTED_MODULE_7__[\"default\"].render(\"**Title.** \".concat(talk.title || '')) + _util_markdown__WEBPACK_IMPORTED_MODULE_7__[\"default\"].render(talk[\"abstract\"] || '')\n        }, pages, state));\n      });\n    });\n  }); // all reamaining routes are user-inputted and dealt with in the same manner\n\n  router.get('*', function (req, res, next) {\n    // create the authorization state\n    var state = {\n      loggedIn: Object(_auth__WEBPACK_IMPORTED_MODULE_6__[\"loggedIn\"])(req),\n      authorized: Object(_auth__WEBPACK_IMPORTED_MODULE_6__[\"authorized\"])(req)\n    }; // perform a search for the path on the database\n\n    db.get('SELECT id, title, body FROM pages WHERE path = ?', req.path, function (err, page) {\n      // errors go to error parser\n      if (err) return next(err); // no match redirects to 404\n\n      if (!page && req.path !== '/404') return res.redirect('/404');\n      if (!page && req.path === '/404') return next(); // otherwise, build the page\n\n      db.all('SELECT id, title, path FROM pages WHERE position >= 0 ORDER BY position;', function (err, pages) {\n        // errors go to error parser\n        if (err) return next(err); // make the page\n\n        res.send(generateHtml(page, pages, state));\n      });\n    });\n  }); // our 404 page (if not established in SQLITE)\n\n  router.get('/404', function (req, res) {\n    // create the authorization state\n    var state = {\n      loggedIn: Object(_auth__WEBPACK_IMPORTED_MODULE_6__[\"loggedIn\"])(req),\n      authorized: Object(_auth__WEBPACK_IMPORTED_MODULE_6__[\"authorized\"])(req)\n    }; // immediately go to links, as we have 404'd\n\n    db.all('SELECT id, title, path, position FROM pages WHERE position >= 0 ORDER BY position;', function (err, pages) {\n      // errors go to error parser\n      if (err) return next(err); // make the page\n\n      res.send(generateHtml({\n        id: -1,\n        title: '404 Path Not Found',\n        body: '# 404 Page Not Found.\\n\\n$\\\\text{route requested} \\\\not\\\\in \\\\text{site routes}$'\n      }, pages, state));\n    });\n  }); // return router\n\n  return router;\n});\n\n//# sourceURL=webpack:///./src/server/routes.js?");

/***/ }),

/***/ "./src/server/sessions.js":
/*!********************************!*\
  !*** ./src/server/sessions.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express_session__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express-session */ \"express-session\");\n/* harmony import */ var express_session__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express_session__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var connect_sqlite3__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! connect-sqlite3 */ \"connect-sqlite3\");\n/* harmony import */ var connect_sqlite3__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(connect_sqlite3__WEBPACK_IMPORTED_MODULE_1__);\n/**\n * server/sessions.js\n *\n * This module creates middleware for storing user sessions.\n */\n// module dependencies: npm packages\n\n // create sqlite3 session store\n\nvar SQLiteStore = connect_sqlite3__WEBPACK_IMPORTED_MODULE_1___default()(express_session__WEBPACK_IMPORTED_MODULE_0___default.a); // middleware\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (function () {\n  return express_session__WEBPACK_IMPORTED_MODULE_0___default()({\n    secret: \"ratsRATSRAAAATS\",\n    saveUninitialized: false,\n    resave: false,\n    cookie: {\n      maxAge: 1000 * 60 * 60 * 24 * 30\n    },\n    store: new SQLiteStore({\n      dir: process.cwd(),\n      db: 'database.db'\n    })\n  });\n});\n\n//# sourceURL=webpack:///./src/server/sessions.js?");

/***/ }),

/***/ "./src/util/markdown.js":
/*!******************************!*\
  !*** ./src/util/markdown.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var markdown_it__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! markdown-it */ \"markdown-it\");\n/* harmony import */ var markdown_it__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(markdown_it__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _iktakahiro_markdown_it_katex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @iktakahiro/markdown-it-katex */ \"@iktakahiro/markdown-it-katex\");\n/* harmony import */ var _iktakahiro_markdown_it_katex__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_iktakahiro_markdown_it_katex__WEBPACK_IMPORTED_MODULE_1__);\n/**\n * util/markdown.js\n *\n * This module exports a MarkdownIt parser which is to be used on both the \n * server and client end for generating HTML from markdown.\n */\n// module dependencies: npm packages\n\n // export the parser\n\nvar md = new markdown_it__WEBPACK_IMPORTED_MODULE_0___default.a({\n  html: true,\n  xhtmlout: true\n});\nmd.use(_iktakahiro_markdown_it_katex__WEBPACK_IMPORTED_MODULE_1___default.a, {\n  delimiters: [{\n    left: '\\\\[',\n    right: '\\\\]',\n    display: true\n  }]\n});\n/* harmony default export */ __webpack_exports__[\"default\"] = (md);\n\n//# sourceURL=webpack:///./src/util/markdown.js?");

/***/ }),

/***/ "./src/util/react-components.jsx":
/*!***************************************!*\
  !*** ./src/util/react-components.jsx ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_responsive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-responsive */ \"react-responsive\");\n/* harmony import */ var react_responsive__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_responsive__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _ucsb_logo_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ucsb-logo.svg */ \"./src/util/ucsb-logo.svg\");\n/* harmony import */ var _ucsb_logo_svg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_ucsb_logo_svg__WEBPACK_IMPORTED_MODULE_2__);\nfunction _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }\n\nfunction _nonIterableSpread() { throw new TypeError(\"Invalid attempt to spread non-iterable instance\"); }\n\nfunction _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === \"[object Arguments]\") return Array.from(iter); }\n\nfunction _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }\n\nfunction _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }\n\nfunction _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }\n\nfunction _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }\n\n/**\n * util/react-components.jsx\n *\n * This is the module for the React component corresponding to the UI of the \n * site\n */\n// module dependencies: npm packages\n\n // module dependencies: project modules\n\n // simple button that includes login logic\n\nvar UserButton = function UserButton(_ref) {\n  var loggedIn = _ref.loggedIn,\n      props = _objectWithoutProperties(_ref, [\"loggedIn\"]);\n\n  return loggedIn ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"a\", _extends({\n    href: \"/auth\"\n  }, props), \"Developer Panel\") : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"a\", _extends({\n    href: \"/auth/login\"\n  }, props), \"Login\");\n}; // the top navigator\n\n\nvar TopNav = function TopNav(_ref2) {\n  var id = _ref2.id,\n      pages = _ref2.pages,\n      loggedIn = _ref2.loggedIn;\n  // turn the pages into links\n  var links = pages.map(function (p) {\n    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"a\", {\n      href: p.path,\n      key: p.id,\n      className: p.id == id ? 'current' : ''\n    }, p.title);\n  });\n  var siam = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"a\", {\n    key: \"siam\",\n    href: \"https://www.siam.org/\"\n  }, \"SIAM Homepage\");\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    id: \"topnav\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    id: \"eyebrow\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"inner\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"left\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"a\", {\n    href: \"https://www.ucsb.edu\"\n  }, \"University of California, Santa Barbara\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"right\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(UserButton, {\n    loggedIn: loggedIn\n  })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    id: \"brand-box\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    id: \"brand\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"span\", {\n    className: \"logo\",\n    dangerouslySetInnerHTML: {\n      __html: _ucsb_logo_svg__WEBPACK_IMPORTED_MODULE_2___default.a\n    }\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"span\", {\n    className: \"title\"\n  }, \"SIAM Seminar\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"links\"\n  }, [].concat(_toConsumableArray(links), [siam])))));\n}; // the root of the site\n\n\nvar Root = function Root(_ref3) {\n  var body = _ref3.body,\n      props = _objectWithoutProperties(_ref3, [\"body\"]);\n\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(TopNav, props), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    id: \"content\",\n    dangerouslySetInnerHTML: {\n      __html: body\n    }\n  }));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Root);\n\n//# sourceURL=webpack:///./src/util/react-components.jsx?");

/***/ }),

/***/ "./src/util/ucsb-logo.svg":
/*!********************************!*\
  !*** ./src/util/ucsb-logo.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"<svg viewBox=\\\"0 0 338 24\\\" version=\\\"1.1\\\" xmlns=\\\"http://www.w3.org/2000/svg\\\" xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"><title>Logo/Navy/UC Santa Barbara@1x</title><desc>Created with Sketch.</desc><defs><path d=\\\"M66.2382323,9.87707069 C63.5288938,9.15508625 61.2864813,8.42026135 61.2864813,6.73874384 C61.2864813,5.67765435 62.1141078,4.09827691 64.392707,4.09827691 C66.6625514,4.09827691 68.3650805,4.88446368 69.5925125,6.50352983 L72.8294774,3.68446368 C70.7744191,0.480767185 67.0232518,-0.000166666667 64.9646914,-0.000166666667 C59.8384268,-0.000166666667 56.2576875,2.96189559 56.2576875,7.20275162 C56.2576875,11.4214287 60.1144969,13.0597555 63.9076875,14.122596 C67.1218899,15.0237633 68.3919288,15.4580045 68.3919288,17.4039189 C68.3919288,18.944775 67.0244191,19.9019734 64.8216953,19.9019734 C61.2987381,19.9019734 59.359244,17.5667594 58.7948471,16.7624792 L55.3115786,19.7438022 C57.0788938,22.5687049 60.1337576,23.9998333 64.392707,23.9998333 C69.7080767,23.9998333 73.4213062,20.9344637 73.4213062,16.5465259 C73.4213062,12.1270707 69.7127459,10.7869929 66.2382323,9.87707069 L66.2382323,9.87707069 Z M42.6847304,15.9681213 C42.0964035,19.7554754 39.4524346,21.6821291 35.6323957,21.6821291 C30.6036019,21.6821291 27.6001,18.0903003 27.6001,12.1480824 L27.6001,11.8866038 C27.6001,6.00975551 30.8318121,2.25508625 35.6644969,2.25508625 C39.2236409,2.25508625 41.6399833,3.75742088 42.5866759,7.64224578 L44.9703335,7.64224578 L44.9709171,0.586409209 L42.4232518,0.586409209 L42.4232518,3.13640921 C41.2150805,0.981545396 38.6685825,0.00216796368 35.7298665,0.00216796368 C29.2973763,0.00216796368 24.6619677,4.99769326 24.6619677,11.9192886 L24.6619677,12.1801835 C24.6619677,19.3638411 28.8076875,23.9998333 35.5664424,23.9998333 C40.7014619,23.9998333 44.377921,21.4387438 45.277921,16.2360201 L42.6847304,15.9681213 Z M10.9962089,23.9998333 C5.64115058,23.9998333 2.60496381,20.8655921 2.60496381,15.1842691 L2.6119677,2.80722633 L0.0001,2.80722633 L0.0001,0.586409209 L8.39076148,0.586409209 L8.39076148,2.80722633 L5.45262918,2.80722633 L5.44562529,14.8574209 C5.44562529,19.7554754 7.17617004,21.6494442 11.0609949,21.6494442 C14.783563,21.6494442 17.0037965,20.1471096 17.0037965,14.4336855 L17.0102167,2.80722633 L14.0720844,2.80722633 L14.0720844,0.586409209 L22.4633296,0.586409209 L22.4633296,2.80722633 L19.8508782,2.80722633 L19.844458,14.8253197 C19.844458,20.506059 17.0037965,23.9998333 10.9962089,23.9998333 L10.9962089,23.9998333 Z M133.550878,5.07531971 L127.188427,5.07531971 L127.188427,0.582323606 L145.422474,0.582323606 L145.422474,5.07531971 L138.63045,5.07531971 L138.63045,23.635631 L133.550878,23.635631 L133.550878,5.07531971 Z M101.793485,0.582323606 L108.696404,0.582323606 L118.497765,16.6025571 L118.562551,16.6025571 L118.562551,0.582323606 L123.642707,0.582323606 L123.642707,23.635631 L117.0001,23.635631 L106.937843,7.22493061 L106.873057,7.22493061 L106.873057,23.635631 L101.793485,23.635631 L101.793485,0.582323606 Z M155.142123,7.2938022 L152.341734,14.4570318 L157.975197,14.4570318 L155.142123,7.2938022 Z M153.188621,0.586409209 L157.388621,0.586409209 L167.41761,23.6391329 L161.686676,23.6391329 L159.700489,18.7550863 L150.746598,18.7550863 L148.825197,23.6391329 L143.225003,23.6391329 L153.188621,0.586409209 Z M86.7041856,7.2938022 L83.9043802,14.4570318 L89.5366759,14.4570318 L86.7041856,7.2938022 Z M84.7506837,0.586409209 L88.9506837,0.586409209 L98.979672,23.6391329 L93.2487381,23.6391329 L91.2625514,18.7550863 L82.3086603,18.7550863 L80.3872595,23.6391329 L74.787065,23.6391329 L84.7506837,0.586409209 Z M228.115664,10.1233742 L231.110411,10.1233742 C231.566832,10.1233742 232.0501,10.1070318 232.560217,10.074347 C233.06975,10.0410785 233.530839,9.94419131 233.944069,9.78135084 C234.355547,9.61792672 234.698154,9.36345201 234.969555,9.01559209 C235.240956,8.66889948 235.376365,8.18037808 235.376365,7.55061154 C235.376365,6.96461933 235.257298,6.49827691 235.018582,6.14983333 C234.779283,5.80372438 234.475197,5.53699287 234.106326,5.35255707 C233.73687,5.16870493 233.314302,5.0438022 232.836287,4.9778489 C232.358855,4.91306291 231.892512,4.87979442 231.436092,4.87979442 L228.115664,4.87979442 L228.115664,10.1233742 Z M223.035509,0.582323606 L231.957882,0.582323606 C233.129283,0.582323606 234.241734,0.696720493 235.294653,0.92493061 C236.347571,1.15255707 237.26975,1.53251816 238.06294,2.06364656 C238.854964,2.59594228 239.484147,3.3126738 239.951656,4.21325746 C240.417999,5.11384112 240.651462,6.23738197 240.651462,7.58329637 C240.651462,9.21111738 240.22831,10.5949695 239.381423,11.7348528 C238.535119,12.8741524 237.297765,13.5967205 235.669361,13.9002224 L241.531034,23.635631 L235.441734,23.635631 L230.623057,14.420845 L228.115664,14.420845 L228.115664,23.635631 L223.035509,23.635631 L223.035509,0.582323606 Z M182.193485,19.3381602 L186.751851,19.3381602 C187.142318,19.3381602 187.538621,19.2943859 187.940178,19.2068372 C188.341734,19.1210396 188.705353,18.9687049 189.031618,18.7515843 C189.356715,18.5344637 189.622279,18.2531407 189.828894,17.9052808 C190.034341,17.5580045 190.138232,17.1348528 190.138232,16.6352419 C190.138232,16.0924403 190.002824,15.6529462 189.731423,15.3161757 C189.459439,14.979989 189.117415,14.7255143 188.705353,14.5510006 C188.292707,14.3776543 187.847376,14.2585882 187.370528,14.1932185 C186.893096,14.1272652 186.447765,14.0957477 186.035119,14.0957477 L182.193485,14.0957477 L182.193485,19.3381602 Z M182.193485,9.79769326 L185.872863,9.79769326 C186.263913,9.79769326 186.648544,9.75391894 187.028505,9.66695396 C187.407882,9.58057263 187.749905,9.43874384 188.053991,9.24321855 C188.358077,9.04886057 188.602046,8.78796563 188.787065,8.4622847 C188.970917,8.13718742 189.063135,7.74613684 189.063135,7.2897166 C189.063135,6.81286835 188.960411,6.41656485 188.753796,6.10138975 C188.548349,5.78738197 188.282785,5.54282944 187.95652,5.36889948 C187.630839,5.19555318 187.261384,5.07123411 186.849322,4.99477497 C186.436676,4.91831582 186.035119,4.87979442 185.644653,4.87979442 L182.193485,4.87979442 L182.193485,9.79769326 Z M177.11333,0.582323606 L185.709439,0.582323606 C186.708077,0.582323606 187.712551,0.652946174 188.721695,0.793607652 C189.731423,0.935436446 190.63726,1.21734306 191.440956,1.64049481 C192.243485,2.06364656 192.894847,2.65605901 193.394458,3.41598119 C193.893485,4.17531971 194.143291,5.18504734 194.143291,6.4434131 C194.143291,7.74613684 193.779672,8.82590337 193.051851,9.68329637 C192.324614,10.5418567 191.364497,11.1541135 190.170917,11.5229851 L190.170917,11.5889384 C190.930256,11.696915 191.619555,11.9087827 192.238232,12.2233742 C192.856909,12.5379656 193.388621,12.939522 193.833952,13.4274598 C194.278699,13.9165649 194.620723,14.4862147 194.859439,15.1375765 C195.098154,15.7883547 195.217221,16.4834909 195.217221,17.2218178 C195.217221,18.4375765 194.956909,19.4513898 194.436287,20.2661757 C193.915664,21.0792108 193.242707,21.7358256 192.417415,22.2360201 C191.592123,22.7350473 190.664691,23.0934131 189.633368,23.3105337 C188.602046,23.5276543 187.576559,23.635631 186.556909,23.635631 L177.11333,23.635631 L177.11333,0.582323606 Z M323.9108,7.2938022 L321.111579,14.4570318 L326.743874,14.4570318 L323.9108,7.2938022 Z M321.957882,0.586409209 L326.157882,0.586409209 L336.18687,23.6391329 L330.455937,23.6391329 L328.46975,18.7550863 L319.515275,18.7550863 L317.594458,23.6391329 L311.994263,23.6391329 L321.957882,0.586409209 Z M208.341345,7.2938022 L205.540956,14.4570318 L211.173835,14.4570318 L208.341345,7.2938022 Z M206.38726,0.586409209 L210.58726,0.586409209 L220.616248,23.6391329 L214.885898,23.6391329 L212.899711,18.7550863 L203.945236,18.7550863 L202.023835,23.6391329 L196.423641,23.6391329 L206.38726,0.586409209 Z M296.728699,10.1262925 L299.72403,10.1262925 C300.179867,10.1262925 300.663135,10.1093664 301.173252,10.0778489 C301.683368,10.0445804 302.144458,9.9471096 302.557688,9.78426913 C302.969166,9.62142866 303.311189,9.36695396 303.58259,9.01851038 C303.853991,8.67181777 303.989983,8.18388003 303.989983,7.55352983 C303.989983,6.96812127 303.870333,6.5011952 303.631618,6.15333528 C303.392318,5.80664267 303.088816,5.54049481 302.719944,5.35605901 C302.350489,5.17162322 301.927337,5.04672049 301.449905,4.98135084 C300.972474,4.91656485 300.505547,4.88329637 300.049711,4.88329637 L296.728699,4.88329637 L296.728699,10.1262925 Z M291.649127,0.585825551 L300.570917,0.585825551 C301.742902,0.585825551 302.854769,0.699055123 303.908271,0.927848898 C304.961189,1.15605901 305.883368,1.5360201 306.675975,2.06714851 C307.468582,2.59944423 308.097765,3.31617575 308.565275,4.2167594 C309.031034,5.11734306 309.265081,6.24088392 309.265081,7.58621466 C309.265081,9.21403567 308.841345,10.5984715 307.995042,11.7377711 C307.148154,12.8776543 305.911384,13.5996388 304.282979,13.9025571 L310.144653,23.6391329 L304.054769,23.6391329 L299.235509,14.424347 L296.728699,14.424347 L296.728699,23.6391329 L291.649127,23.6391329 L291.649127,0.585825551 Z M250.80652,19.3422458 L255.364886,19.3422458 C255.755937,19.3422458 256.151656,19.2984715 256.553213,19.2109228 C256.955353,19.1251252 257.318972,18.9727905 257.644069,18.7556699 C257.970333,18.5385493 258.235314,18.2566427 258.442512,17.9093664 C258.64796,17.5620901 258.751851,17.1383547 258.751851,16.6393275 C258.751851,16.0965259 258.615859,15.6570318 258.345042,15.3202613 C258.073057,14.9840746 257.731034,14.7290162 257.318972,14.5550863 C256.906326,14.3817399 256.460995,14.2626738 255.983563,14.1967205 C255.506131,14.1313508 255.0608,14.0992497 254.648738,14.0992497 L250.80652,14.0992497 L250.80652,19.3422458 Z M250.80652,9.8011952 L254.485898,9.8011952 C254.876948,9.8011952 255.261579,9.75800454 255.642123,9.6704559 C256.021501,9.58465824 256.363524,9.44282944 256.66761,9.24730415 C256.971695,9.05294617 257.215664,8.79205123 257.4001,8.4663703 C257.584536,8.14068936 257.676754,7.74963878 257.676754,7.2938022 C257.676754,6.81695396 257.573446,6.42065045 257.367415,6.10547536 C257.1608,5.79146757 256.89582,5.54691505 256.570139,5.37298508 C256.243874,5.19963878 255.874419,5.07473606 255.46294,4.99886057 C255.049711,4.92240143 254.648738,4.88388003 254.258271,4.88388003 L250.80652,4.88388003 L250.80652,9.8011952 Z M245.726365,0.586409209 L254.323057,0.586409209 C255.321112,0.586409209 256.32617,0.657031777 257.335314,0.797693256 C258.345042,0.939522049 259.250295,1.22142866 260.053991,1.64458042 C260.85652,2.06773217 261.507882,2.66014462 262.007493,3.4200668 C262.50652,4.17882166 262.756326,5.18913294 262.756326,6.4474987 C262.756326,7.74963878 262.392707,8.82998898 261.66547,9.68738197 C260.938232,10.5453586 259.977532,11.1581991 258.784536,11.5270707 L258.784536,11.5924403 C259.543291,11.7010006 260.233174,11.9128684 260.851267,12.2268761 C261.470528,12.5414676 262.001656,12.9436077 262.446987,13.4315454 C262.892318,13.9206505 263.234341,14.4903003 263.473057,15.1416621 C263.711189,15.7924403 263.830839,16.4875765 263.830839,17.2253197 C263.830839,18.4416621 263.570528,19.4554754 263.049322,20.2702613 C262.529283,21.0832964 261.855742,21.7399112 261.03045,22.2401057 C260.205742,22.7391329 259.277726,23.096915 258.246987,23.3140357 C257.215664,23.5317399 256.189594,23.6397166 255.169361,23.6397166 L245.726365,23.6397166 L245.726365,0.586409209 Z M276.953796,7.2938022 L274.153991,14.4570318 L279.787454,14.4570318 L276.953796,7.2938022 Z M275.000878,0.586409209 L279.200878,0.586409209 L289.229867,23.6391329 L283.498933,23.6391329 L281.51333,18.7550863 L272.558855,18.7550863 L270.637454,23.6391329 L265.036676,23.6391329 L275.000878,0.586409209 Z\\\" id=\\\"path-1\\\"></path></defs><g id=\\\"Symbols\\\" stroke=\\\"none\\\" stroke-width=\\\"1\\\" fill=\\\"none\\\" fill-rule=\\\"evenodd\\\"><g id=\\\"Logo/Navy/UC-Santa-Barbara\\\"><mask id=\\\"mask-2\\\" fill=\\\"white\\\"><use xlink:href=\\\"#path-1\\\"></use></mask><use id=\\\"logotype\\\" fill=\\\"#FFFFFF\\\" xlink:href=\\\"#path-1\\\"></use><g id=\\\"UI/Color/Flat/Primary\\\" mask=\\\"url(#mask-2)\\\" fill=\\\"#003660\\\"><rect id=\\\"Base\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"338\\\" height=\\\"24\\\"></rect></g></g></g></svg>\"\n\n//# sourceURL=webpack:///./src/util/ucsb-logo.svg?");

/***/ }),

/***/ "@iktakahiro/markdown-it-katex":
/*!************************************************!*\
  !*** external "@iktakahiro/markdown-it-katex" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@iktakahiro/markdown-it-katex\");\n\n//# sourceURL=webpack:///external_%22@iktakahiro/markdown-it-katex%22?");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"body-parser\");\n\n//# sourceURL=webpack:///external_%22body-parser%22?");

/***/ }),

/***/ "connect-sqlite3":
/*!**********************************!*\
  !*** external "connect-sqlite3" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"connect-sqlite3\");\n\n//# sourceURL=webpack:///external_%22connect-sqlite3%22?");

/***/ }),

/***/ "crypto":
/*!*************************!*\
  !*** external "crypto" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"crypto\");\n\n//# sourceURL=webpack:///external_%22crypto%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "express-file-store":
/*!*************************************!*\
  !*** external "express-file-store" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express-file-store\");\n\n//# sourceURL=webpack:///external_%22express-file-store%22?");

/***/ }),

/***/ "express-session":
/*!**********************************!*\
  !*** external "express-session" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express-session\");\n\n//# sourceURL=webpack:///external_%22express-session%22?");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"http\");\n\n//# sourceURL=webpack:///external_%22http%22?");

/***/ }),

/***/ "markdown-it":
/*!******************************!*\
  !*** external "markdown-it" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"markdown-it\");\n\n//# sourceURL=webpack:///external_%22markdown-it%22?");

/***/ }),

/***/ "morgan":
/*!*************************!*\
  !*** external "morgan" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"morgan\");\n\n//# sourceURL=webpack:///external_%22morgan%22?");

/***/ }),

/***/ "passport":
/*!***************************!*\
  !*** external "passport" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"passport\");\n\n//# sourceURL=webpack:///external_%22passport%22?");

/***/ }),

/***/ "passport-local":
/*!*********************************!*\
  !*** external "passport-local" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"passport-local\");\n\n//# sourceURL=webpack:///external_%22passport-local%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "randomstring":
/*!*******************************!*\
  !*** external "randomstring" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"randomstring\");\n\n//# sourceURL=webpack:///external_%22randomstring%22?");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");\n\n//# sourceURL=webpack:///external_%22react%22?");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-dom/server\");\n\n//# sourceURL=webpack:///external_%22react-dom/server%22?");

/***/ }),

/***/ "react-responsive":
/*!***********************************!*\
  !*** external "react-responsive" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-responsive\");\n\n//# sourceURL=webpack:///external_%22react-responsive%22?");

/***/ }),

/***/ "sqlite3":
/*!**************************!*\
  !*** external "sqlite3" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"sqlite3\");\n\n//# sourceURL=webpack:///external_%22sqlite3%22?");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"url\");\n\n//# sourceURL=webpack:///external_%22url%22?");

/***/ })

/******/ });