/**
 * util/react-components.jsx
 *
 * This is the module for the React component corresponding to the UI of the 
 * site
 */

// module dependencies: npm packages
import React from 'react';
import { useMediaQuery } from 'react-responsive';

// module dependencies: project modules
import logo from './ucsb-logo.svg';

// simple button that includes login logic
const UserButton = ({ loggedIn, ...props }) => (
  loggedIn ?
    <a href="/auth" { ...props }>Developer Panel</a> :
    <a href="/auth/login" { ...props }>Login</a>
)

// the top navigator
const TopNav = ({ id, pages, loggedIn }) => {
  // turn the pages into links
  const links = pages.map((p) => (
    <a 
      href={p.path} 
      key={p.id} 
      className={p.id == id ? 'current' : ''}
    >
      { p.title }
    </a>
  ));
  const siam = <a key="siam" href="https://www.siam.org/">SIAM Homepage</a>;

  return <>
    <div id="topnav">
      <div id="eyebrow"> 
        <div className="inner">
          <div className="left">
            <a href="https://www.ucsb.edu">
              University of California, Santa Barbara
            </a>
          </div>
          <div className="right">
            <UserButton loggedIn={loggedIn} />
          </div>
        </div>
      </div>
      <div id="brand-box">
        <div id="brand">
          <span className="logo" dangerouslySetInnerHTML={{__html: logo}} />
          <span className="title">SIAM Seminar</span>
        </div>
        <div className="links">{ [...links, siam ] }</div>
      </div>
    </div>
  </>;
};

// the root of the site
const Root = ({ body, ...props }) => {
  return <>
    <TopNav { ...props } />
    <div id="content" dangerouslySetInnerHTML={{ __html: body }} />
  </>;
};
export default Root;
