/**
 * util/markdown.js
 *
 * This module exports a MarkdownIt parser which is to be used on both the 
 * server and client end for generating HTML from markdown.
 */

// module dependencies: npm packages
import MarkdownIt from 'markdown-it';
import mdKatexPlugin from '@iktakahiro/markdown-it-katex';

// export the parser
const md = new MarkdownIt({ html: true, xhtmlout: true });
md.use(mdKatexPlugin, {
  delimiters: [{ left: '\\[', right: '\\]', display: true }]
});
export default md;
