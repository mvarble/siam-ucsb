/**
 * server/index.js
 *
 * This is the entry point for deploying the http server which serves our 
 * webapp.
 */

// module dependencies: project modules
import deployHttpServer from './http';
import app from './app';

// deploy the server
deployHttpServer(app);
