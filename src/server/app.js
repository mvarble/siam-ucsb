/**
 * server/app.js
 *
 * This module exports the Express webapp
 */

// module dependencies: npm packages
import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import passport from 'passport';
import path from 'path';

// module dependencies: project modules
import sessionMiddleware from './sessions';
import { authRouter } from './auth';
import siteRouter from './routes';
import filestoreMiddleware from './filestore';
import dbAPI from './db';

// create the Express app
const app = express();

// body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// logger middleware
app.use(morgan('tiny'));

// session middleware
app.use(sessionMiddleware());

// authentication middleware
app.use(passport.initialize());
app.use(passport.session());
app.use('/auth', authRouter());

// static middleware
app.use('/static/uploads', filestoreMiddleware());
app.use('/static', express.static(path.join(process.cwd(), 'dist')));
app.use('/static/*', (req, res) => res.status(404).send('404'));

// database API middleware
app.use('/db', dbAPI());

// routes
app.use('/', siteRouter());

export default app;
