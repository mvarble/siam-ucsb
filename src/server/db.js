/**
 * server/db.js
 *
 * This module is responsible for creating the SQLITE database API
 */

// module dependencies: npm packages
import { Router } from 'express';
import { Database } from 'sqlite3';
import path from 'path';
import { parse } from 'url';

export default () => {
  // router we eventually export
  const router = Router();

  // connect to the sqlite3 database
  const db = new Database(path.join(process.cwd(), 'database.db'));

  // a simple function we repeatedly use
  const cb = (res, next, err) => {
    if (err) return next(err);
    res.send();
  };

  // posting pages
  router.post('/pages', (req, res, next) => {
    // extract the body if it is sufficient
    if (!req.body) return next(Error('The post request to /pages has no body'));
    const { title, path, body, position } = req.body;
    if (!title || !path || !body || !position) {
      return next(Error('The post request to /pages has an insufficient body'));
    }
    // post it to the database
    db.run(
      'INSERT INTO pages (title, path, body, position) VALUES (?, ?, ?, ?)',
      [title, path, body, position],
      err => cb(res, next, err)
    );
  });

  // updating pages
  router.put('/pages', (req, res, next) => {
    // check if there is a body
    if (!req.body) return next(Error('The put request to /pages has no body'));
    if (!req.body.id) return next(Error('The put request to /pages has no id'));

    // get the fields that are requesting to be changed from the request
    const { id, title, path, body, position } = req.body;
    const bodyObj = { title, path, body, position };
    const keys = Object.keys(bodyObj).filter(k => bodyObj[k]);

    // if there are no keys, it was a blank request
    if (!keys.length) return res.send();

    // update any non-blank request
    const setString = keys.map(k => `${k} = (?)`).join(', ');
    db.run(
      `UPDATE pages SET ${setString} WHERE id = (?)`,
      [ ...keys.map(k => bodyObj[k]) , id ],
      err => cb(res, next, err)
    );
  });

  // deleting pages
  router.delete('/pages', (req, res, next) => {
    // check if there is a body
    const noId = 'The delete request to /pages has no id';
    if (!req.body || !req.body.id) return next(Error(noId));

    // if so, try to delete the field
    const { id } = req.body;
    db.run(`DELETE FROM pages WHERE id = (?)`, id, err => cb(res, next, err));
  });

  // getting talks
  router.get('/talks', (req, res, next) => {
    // get the query
    const query = parse(req.url, true).query;

    // our callback
    const callback = (err, talks) => {
      if (err) return next(err);
      res.set('Content-Type', 'application/json');
      res.send(JSON.stringify(talks));
    };

    // if there is no begin/end, grab all of them
    if (!query.begin || !query.end) {
      db.all('SELECT * FROM talks', callback);
    } else {
      db.all(
        'SELECT * FROM talks WHERE date >= ? AND date <= ? ORDER BY date',
        [query.begin, query.end],
        callback
      );
    }
  });

  // posting talks
  router.post('/talks', (req, res, next) => {
    // check if there is a body
    if (!req.body) return next(Error('The post request to /talks has no body'));

    // parse the body
    const { title, speaker, abstract, date } = req.body;
    const bodyObj = { speaker, title, abstract, date };
    const keys = Object.keys(bodyObj).filter(k => bodyObj[k]);

    // if no keys, insert default
    if (!keys.length) {
      db.run('INSERT INTO talks DEFAULT VALUES', [], err => cb(res, next, err));
    } else {
      // create the row
      const setString = keys.join(', ');
      db.run(
        `INSERT INTO talks (${keys.join(', ')}) VALUES 
        (${keys.map(k=>'?').join(', ')})`,
        keys.map(k => bodyObj[k]),
        err => cb(res, next, err)
      );
    }
  });

  // updating talks
  router.put('/talks', (req, res, next) => {
    // check if there is a body
    if (!req.body) return next(Error('The put request to /talks has no body'));
    if (!req.body.id) return next(Error('The put request to /talks has no id'));

    // get the fields that are requesting to be changed from the request
    const { id, title, speaker, abstract, date } = req.body;
    const bodyObj = { title, speaker, abstract, date };
    const keys = Object.keys(bodyObj).filter(k => bodyObj[k]);

    // if there are no keys, it was a blank request
    if (!keys.length) return res.send();

    // update any non-blank request
    const setString = keys.map(k => `${k} = (?)`).join(', ');
    db.run(
      `UPDATE talks SET ${setString} WHERE id = (?)`,
      [ ...keys.map(k => bodyObj[k]) , id ],
      err => cb(res, next, err)
    );
  });

  // deleting talks
  router.delete('/talks', (req, res, next) => {
    // check if there is a body
    const noId = 'The delete request to /talks has no id';
    if (!req.body || !req.body.id) return next(Error(noId));

    // if so, try to delete the field
    const { id } = req.body;
    db.run(`DELETE FROM talks WHERE id = (?)`, id, err => cb(res, next, err));
  });

  return router;
};
