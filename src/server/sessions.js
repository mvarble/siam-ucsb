/**
 * server/sessions.js
 *
 * This module creates middleware for storing user sessions.
 */

// module dependencies: npm packages
import session from 'express-session';
import connectSQL from 'connect-sqlite3';

// create sqlite3 session store
const SQLiteStore = connectSQL(session);

// middleware
export default () => session({
  secret: process.env.CLIENT_SECRET,
  saveUninitialized: false,
  resave: false,
  cookie: { maxAge: 1000 * 60 * 60 * 24 * 30 },
  store: new SQLiteStore({
    dir: process.cwd(),
    db: 'database.db',
  }),
});
