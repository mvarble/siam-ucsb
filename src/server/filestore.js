/**
 * server/filestore.js
 *
 * This exports middleware that will allow for upload/download of files
 */

// module dependencies: npm packages
import { Router } from 'express';
import FileStore from 'express-file-store';
import path from 'path';

export default () => {
  // our router
  const router = Router();

  // the filesystem path for storing the files
  const fileDir = path.join(process.cwd(), 'dist', 'uploads');

  // the routes are handld by express-file-store
  router.use(FileStore('fs', { path: fileDir }).routes);

  // add an 404 handler
  router.use((err, req, res, next) => {
    if (err && err.message === 'File not found') {
      return res.status(404).send('FILE NOT FOUND');
    }
    return next(err);
  });

  return router;
};
