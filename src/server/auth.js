/**
 * server/auth.js
 *
 * This module exports middleware responsible for authenticating users.
 * It uses a local passport strategy and stores the user data into the sqlite3
 * database located at ../../database.db
 */

// module dependencies: npm packages
import { Database } from 'sqlite3';
import passport from 'passport';
import LocalStrategy from 'passport-local';
import { Router } from 'express';
import path from 'path';
import crypto from 'crypto';
import randomstring from 'randomstring';

// connect to the sqlite3 database
const db = new Database(path.join(process.cwd(), 'database.db'));

// this function uses crypto to hash passwords
const hashPassword = (password, salt) => {
  const hash = crypto.createHash('sha256');
  hash.update(password);
  hash.update(salt);
  return hash.digest('hex');
};

// configure passport
passport.use(new LocalStrategy((username, password, done) => {
  // get the salts of the users with matching username
  db.get(
    'SELECT salt FROM users WHERE username = ?',
    username, 
    (err, user) => {
      // if we don't have any matches, call it a day
      if (err || !user) {
        return done(null, false);
      }

      // otherwise, get the password hash and see if there is a corresponding 
      // user
      db.get(
        'SELECT username, id, auth FROM users WHERE username = ? AND password = ?',
        username,
        hashPassword(password, user.salt),
        (err, user) => {
          // if there is a user, return
          if (err || !user) {
            return done(null, false);
          }
          return done(null, user);
        },
      );
    },
  );
}));
passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((user, done) => done(null, user));

// a session checker
const loggedIn = req => (
  (req.session && req.session.passport && req.session.passport.user) ? 
    true : 
    false
);
export { loggedIn };

// an authorization checker
const authorized = req => (
  (loggedIn(req) && req.session.passport.user.auth) ? true : false
);
export { authorized };

// create router middleware
const authRouter = () => {
  // the router we will eventually return
  const router = Router();

  // We use an almost identical form for the login and create pages
  const form = (buttonText, onsubmit) => `
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>User ${buttonText}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://webfonts.brand.ucsb.edu/webfont.min.css" rel="stylesheet">
</head>
<body>
  <form method="post" onsubmit="${onsubmit}">
    <input type="text" name="username" required>
    <input type="password" name="password" required>
    <button type="submit">${buttonText}</button>
  </form>
</body>
</html>
`;

  const authorizationMessage = `It appears this user is not authorized. 
    Send an email to Matthew Varble at 
    <a href="mailto:mvarble@math.ucsb.edu">mvarble@math.ucsb.edu</a> 
    to request elevated priviledges with this account.
  `;

  // home page for authorized users
  router.get('/', (req, res) => {
    if (!loggedIn(req)) return res.redirect('/auth/login');
    if (!authorized(req)) return res.send(authorizationMessage);
    return res.send('<a href="/auth/users">Users</a>');
  });

  // the page listing all users
  router.get('/users', (req, res, next) => {
    if (!loggedIn(req)) return res.redirect('/auth/login');
    if (!authorized(req)) return res.send(authorizationMessage);
    db.all('SELECT id, username, auth FROM users', (err, users) => {
      if (err) return next(err);
      const usersString = users.map(
        ({ id, username, auth }) => `${id}: ${username}; ${auth}`
      ).join('</li><li>');
      res.send(`<ul><li>${usersString}</li></ul>`);
    });
  });

  // configure the create user route
  router.get('/create', (req, res) => {
    if (loggedIn(req)) return res.redirect('/');
    return res.send(form('Create', 'createUser(event)'));
  });
  router.post('/create', (req, res) => {
    const { username, password } = req.body;
    const salt = randomstring.generate();
    db.run(
      'INSERT INTO users(username, password, salt, auth) VALUES(?, ?, ?, 0)',
      [username, hashPassword(password, salt), salt],
      (err) => {
        if (err) {
          return res.status(422).send(err.message);
        } 
        return res.redirect('/');
      },
    );
  });

  // configure the login
  router.get('/login', (req, res) => {
    if (loggedIn(req)) {
      return res.redirect('/');
    }
    return res.send(form('Login', 'loginUser(event)'))
  });
  router.post(
    '/login', 
    passport.authenticate('local'),
    (req, res) => res.redirect('/'),
  );

  // configure the logout
  router.get('/logout', (req, res) => {
    req.session.destroy((err) => {
      if (err) return next(err);
      return res.redirect('/');
    });
  });

  // return the router
  return router;
};

export { authRouter };
