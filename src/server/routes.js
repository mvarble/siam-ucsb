/**
 * server/routes.js
 *
 * This module creates the router middleware for pages the user will visit.
 */

// module dependencies: npm packages
import React from 'react';
import { Router } from 'express';
import { Database } from 'sqlite3';
import path from 'path';
import { renderToString } from 'react-dom/server';
import { parse } from 'url';

// module dependencies: project
import { loggedIn, authorized } from './auth';
import markdown from '../util/markdown';
import Root from '../util/react-components';

// this generates raw html, provided page data
const generateHtml = (page, pages, authState) => {
  const { body, title, ...otherProps } = page;
  const props = { ...otherProps, ...authState, pages };
  const markup = markdown.render(body);
  return (
`<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>${title} | SIAM UCSB</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://webfonts.brand.ucsb.edu/webfont.min.css" rel="stylesheet">
  <link href="/static/css/bundle.css" rel="stylesheet">
</head>
<body>
  <div id="root">${renderToString(<Root body={markup} {...props} />)}</div>
  <script src="/static/js/bundle.js"></script>
</body>
</html>
`);
};

export default () => {
  // the router we eventually return
  const router = Router();

  // every route at this point will be dealt with in our SQLITE database.
  const db = new Database(path.join(process.cwd(), 'database.db'));

  // talks model view
  router.get('/talks', (req, res, next) => {
    // create the authorization state
    const state = { loggedIn: loggedIn(req), authorized: authorized(req) };

    // if there is no id query, we will assume that this may be handled by 
    // user-generated route
    const { id } = parse(req.url, true).query;
    if (!id) return next();

    // perform a search for the talk on the database
    db.get(
      'SELECT date, speaker, title, abstract FROM talks WHERE id = ?',
      id,
      (err, talk) => {
        // deal with errors / 404
        if (err) return next(err);
        if (!talk) return res.redirect('/404');

        // otherwise, build the page
        db.all(
          'SELECT id, title, path FROM pages WHERE position >= 0 ORDER BY position;', 
          (err, pages) => {
            // errors go to error parser
            if (err) return next(err);

            // make the page
            res.send(generateHtml(
              {
                id: -1,
                title: talk.title,
                position: -1,
                body: markdown.render(`# SIAM Seminar: ${talk.date || ''}`) + 
                  markdown.render(`**Speaker.** ${talk.speaker || ''}`) + 
                  markdown.render(`**Title.** ${talk.title || ''}`) + 
                  markdown.render(talk.abstract || ''),
              },
              pages, 
              state
            ));
          }
        );
      }
    );

  });

  // all reamaining routes are user-inputted and dealt with in the same manner
  router.get('*', (req, res, next) => {
    // create the authorization state
    const state = { loggedIn: loggedIn(req), authorized: authorized(req) };

    // perform a search for the path on the database
    db.get(
      'SELECT id, title, body FROM pages WHERE path = ?',
      req.path, 
      (err, page) => {
        // errors go to error parser
        if (err) return next(err);
        // no match redirects to 404
        if (!page && req.path !== '/404') return res.redirect('/404');
        if (!page && req.path === '/404') return next();
        
        // otherwise, build the page
        db.all(
          'SELECT id, title, path FROM pages WHERE position >= 0 ORDER BY position;', 
          (err, pages) => {
            // errors go to error parser
            if (err) return next(err);

            // make the page
            res.send(generateHtml(page, pages, state));
          }
        );
      }
    );
  });

  // our 404 page (if not established in SQLITE)
  router.get('/404', (req, res) => {
    // create the authorization state
    const state = { loggedIn: loggedIn(req), authorized: authorized(req) };

    // immediately go to links, as we have 404'd
    db.all(
      'SELECT id, title, path, position FROM pages WHERE position >= 0 ORDER BY position;', 
      (err, pages) => {
        // errors go to error parser
        if (err) return next(err);

        // make the page
        res.send(generateHtml(
          { 
            id: -1, 
            title: '404 Path Not Found', 
            body: '# 404 Page Not Found.\n\n$\\text{route requested} \\not\\in \\text{site routes}$' 
          }, 
          pages,
          state
        ));
      }
    );
  });

  // return router
  return router;
};
