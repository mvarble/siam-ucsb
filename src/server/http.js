/**
 * server/http.js
 *
 * This module exports the function which deploys an http server with an
 * Express webapp bound.
 */

// module dependencies: npm packages
import http from 'http';

// the function which deploys the server
export default (app) => {
  // set the port of the webapp
  app.set('port', 8666);

  // create the http server
  const server = http.createServer(app);

  // our logger
  const log = (s) => process.stdout.write(`${s}\n`);

  // handle port errors by scooting up a port
  server.on('error', (err) => {
    if (err.code === 'EADDRINUSE') {
      log(`Port unavailable. Are you sure this service isn't already running?`);
    }
  });

  // log server details when all works
  server.on('listening', () => {
    log(`${'='.repeat(80)}`);
    log('SIAM Server');
    log(`Server is listening on port ${server.address().port}`);
    log(`${'='.repeat(80)}`);
  });

  // start the server
  server.listen(8666);
};
