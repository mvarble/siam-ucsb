/**
 * client/index.js
 *
 * This is the entrypoint for the client bundle.
 */

// node modules: npm packages
import fetch from 'node-fetch';

// node modules: project modules
import '../util/index.scss';
import markdown from '../util/markdown.js';

// this will get the username and password from a form submission
const parseForm = (event) => {
  // prevent the usual POST on form submit
  event.preventDefault();

  // disable any future submissions
  const elements = event.target.elements;
  elements[elements.length-1].disabled = true;
  
  // obtain the form data
  return {
    username: elements.username.value,
    password: elements.password.value,
  };
};

// this will return the promise of a fetch corresponding to a POST request
// at the window's location
const post = (body, pathname) => {
  const { origin } = window.location;
  const config = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  };
  return fetch(`${origin}${pathname}`, config);
};

// this is the callback after a POST to /auth/login
const loginCallback = (event) => (res) => {
    const status = res.status;
    const elements = event.target.elements;
    const button = elements[elements.length-1];
    if (status === 200) {
      window.location.assign('/');
    } else if (status === 401) {
      button.disabled = false;
      console.log('FAIL');
    } else {
      console.log(status);
      button.disabled = false;
      console.log('OTHER ERROR');
    }
};

// this is the callback for when a user submits the user creation form
window.createUser = (event) => {
  // parse the form
  let { username, password } = parseForm(event);
  
  // post request
  post({ username, password }, '/auth/create').then((res) => {
    const status = res.status;
    const elements = event.target.elements;
    const button = elements[elements.length-1];
    if (status === 200) {
      post(userData, '/auth/login').then(loginCallback(event));
    } else if (status === 422) {
      console.log('USER EXISTS');
      button.disabled = false;
    } else {
      console.log('OTHER ERROR');
      button.disabled = false;
    }
  });
};

// this is the callback for when a user submits the user login form
window.loginUser = (event) => {
  // parse the form
  let { username, password } = parseForm(event);
  
  // post request
  post({ username, password }, '/auth/login').then(loginCallback(event))
};


// this is the code which adaptively searches for elements which need
// asynchronous work
document.querySelectorAll('[data-type]').forEach((node) => {
  // switch on the type
  if (!node.attributes['data-type']) return; 
  switch (node.attributes['data-type'].value) {
    case 'talks': {
      if (!node.attributes['data-begin'] || !node.attributes['data-end']) {
        break;
      }
      const begin = node.attributes['data-begin'].value;
      const end =  node.attributes['data-end'].value;
      fetch(`http://localhost:8666/db/talks?begin=${begin}&end=${end}`, {})
        .then(res => res.json())
        .then((data) => {
          const rows = data.map(({ date, title, abstract, speaker, id }) => {
            const dateObj = new Date(date);
            const monthDay = dateObj.toLocaleDateString(
              'default', 
              { month: 'long', day: '2-digit', timeZone: 'UTC' }
            );
            const year = dateObj.getFullYear()
            return `
              <a href="/talks?id=${id}" class="talk-row">
                <div class="date">${monthDay}, ${year}</div>
                <div class="content">
                  ${markdown.render(`**Speaker.** ${speaker || ''}`)}
                  ${markdown.render(`**Title.** ${title || ''}`)}
                </div>
              </a>`;
          });
          node.innerHTML = `<div class="talks">\n${rows.join('\n')}\n</div>`;
        });
    }
  }
});

document.querySelectorAll('[aria-hidden]').forEach((node) => {
  if (node.attributes['aria-hidden'].value === "true") {
    node.style['display'] = 'none';
  }
});

