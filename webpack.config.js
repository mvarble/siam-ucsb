/**
 * webpack.js
 *
 * This is our webpack build configuration.
 */

// module dependencies: npm packages
const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const Dotenv = require('dotenv-webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// get build-time process variables
const base = process.cwd();
const build = process.env.BUILD;
const mode = process.env.MODE;

// our plugins
const plugins = [
  new MiniCssExtractPlugin({ filename: path.join('css', '[name].css') }),
];

// our module configuration
const moduleConfig = {
  rules: [
    {
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: ['babel-loader']
    },
    {
      test: /\.svg$/,
      loader: 'svg-inline-loader',
    },
    {
      test: /\.s?css$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader',
      ],
    },
  ]
};

// build configuration for frontend bundle
const frontConfig = {
  mode,
  plugins,
  entry: {
    bundle: path.join(base, 'src', 'client', 'index.js')
  },
  resolve: { extensions: ['*', '.js', '.jsx'] },
  output: {
    filename: path.join('js', '[name].js'),
    path: path.join(base, 'dist'),
    publicPath: '/static'
  },
  module: moduleConfig,
};

// build configuration for backend bundle
const backConfig = {
  mode,
  plugins,
  entry: path.join(base, 'src', 'server', 'index.js'),
  resolve: { extensions: ['*', '.js', '.jsx'] },
  output: {
    filename: 'index.js',
    path: base
  },
  target: 'node',
  module: moduleConfig,
  externals: [nodeExternals()],
  plugins: [ new Dotenv() ]
};

// export the configuration
if (build === 'front') {
  module.exports = frontConfig;
} else {
  module.exports = backConfig;
}
