# SIAM UCSB Website

This project contains the contents for building and deploying the http server for the website corresponding to UCSB's SIAM chapter.
It is built with standard javascript frameworks and a SQLite database:
  - Web app: [Express](https://expressjs.com/)
  - Authentication: [Passport](http://www.passportjs.org/)
  - Database: [SQLite](https://sqlite.org/index.html) and [Node.js interface](https://www.npmjs.com/package/sqlite3)
  - Frontend: [React](https://reactjs.org/)
  - Forms: [Formik](https://jaredpalmer.com/formik/)

For answers to any questions pertaining to the site, feel free to <a href="mailto:mvarble@math.ucsb.edu">email Matthew Varble</a>.
